#!/usr/bin/env bash
DATABASE_NAME=starter
DATABASE_USER=vagrant
DATABASE_PWD=vagrant

sudo -u postgres psql -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
sudo -u postgres psql -c "CREATE DATABASE $DATABASE_NAME WITH OWNER $DATABASE_USER ENCODING 'UTF8';"