#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings

import re

CIF_TEST_KEY = '753217532'


class MonetaryField(models.DecimalField):
    """
    In order to use the same precision for all the monetary fields; we're only
    overriding the constructor, so that we specify the minimum desired precision
    here.
    """
    def __init__(self, verbose_name=None, name=None, max_digits=settings.MAX_MONETARY_DIGITS,
                 decimal_places=settings.MAX_MONETARY_PRECISION, **kwargs):
        super(MonetaryField, self).__init__(verbose_name, name, max_digits, decimal_places, **kwargs)


class QuantityField(models.DecimalField):
    """
    In order to use the same precision for all the quantity / value fields.
    """
    def __init__(self, verbose_name=None, name=None, max_digits=settings.MAX_QUANTITY_DIGITS,
                 decimal_places=settings.MAX_QUANTITY_PRECISION, **kwargs):
        super(QuantityField, self).__init__(verbose_name, name, max_digits, decimal_places, **kwargs)


class Organization(models.Model):
    """
    Models an organization - firmă sub legislația românească
    For convenience, we've added the validation utility function for the CIF.
    """
    class Meta:
        abstract = True

    cif = models.CharField(max_length=10)
    name = models.CharField(max_length=settings.MAX_NAME_FIELD_LENGTH, blank=False)
    is_vat = models.BooleanField(default=False)
    address = models.TextField()

    @classmethod
    def compute_control_digit(cls, _on):
        """
        compute the control sum by using tuple comprehension, over the reverse
        order strings of the test key and the order number (the first digits of
        the CIF minus the last control figure). zip ensures that we stop when
        the shortest list is traversed, by giving back to us a list of tuples
        from the combined lists
        """
        if not isinstance(_on, str):
            raise ValueError("Expecting a string")
        if not re.match(r'\d{5,9}', _on):
            raise ValueError("Expecting 5 or 9 digits")

        _cs = sum([int(x) * int(y) for x, y in zip(_on[::-1], CIF_TEST_KEY[::-1])])
        _cd = str((_cs * 10) % 11)[-1:]

        return _cd

    @classmethod
    def is_valid_cif(cls, cif):
        """
        Validation of a certain CIF, based on the explanations from here:
        http://ro.wikipedia.org/wiki/Cod_de_Identificare_Fiscal%C4%83
        """
        try:
            # get the cif components: control digit and order number
            _cd = int(cif[-1:])
            _on = cif[:-1]
            _cs = cls.compute_control_digit(_on)

            return _cs == _cd

        except ValueError:
            return False

    @classmethod
    def make_cif(cls, _on):
        """
        Given a certain order number, generate a valid cif by appending the
        control digit to it.
        """
        return _on + cls.compute_control_digit(_on)

    @classmethod
    def strip_name(cls, name):
        """
        Will strip out prefixes and suffixes from the fully qualified company
        name, base on the RO nomenclature
        """
        result = re.sub(r'(?i)S\.?R\.?L\.?|S\.?A\.?$', '', name)
        result = re.sub(r'(?i)S\.?C\.?|P\.?F\.?A\.?|I\.?I\.?', '', result)

        return result.strip()

    @classmethod
    def make_identifier(cls, name):
        """
        From a given name, we will try to remove all the invalid characters so
        that we've got a valid identifier
        """
        result = re.sub(r'[^\w\d_]', '_', name)
        result = re.sub(r'^(\d)', r'_\1', result)

        return result
