# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.contrib.auth.models import User, Group
from django.conf import settings

import datetime


def create_test_users(apps, schema_editor):
    # create couple of groups
    Group.objects.bulk_create([
        Group(name='Accountants'),
        Group(name='Engineers'),
        Group(name='Administrators'),
    ])
    Profile = apps.get_model('users', 'Profile')
    # create the administrator
    user = User.objects.create_superuser(
        username='admin',
        password='LetMeIn',
        first_name='John',
        last_name='Doe',
        email='john.doe@nowhere.com',
    )
    user.groups.add(Group.objects.get(name='Administrators'))
    profile = user.profile
    profile.a_number = 1
    profile.a_string = 'Ana are mere'
    profile.a_date = datetime.date.today()
    profile.save()
    # create "couple" of users
    engineers_group = Group.objects.get(name='Engineers')
    for i in range(1, 100):
        user = User.objects.create_user(
            username='dummy_user.%d' % i,
            password='password',
            first_name='Dummy',
            last_name='User %d' % i,
            email='dummy.user.%d@nowhere.com' % i,
        )
        user.groups.add(engineers_group)
        profile = user.profile
        profile.a_number = i
        profile.a_string = user.get_full_name()
        profile.a_date = datetime.date.today()
        profile.save()


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.RunPython(create_test_users)
    ]
