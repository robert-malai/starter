#!/usr/bin/python
# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.test import APITestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group

import datetime

dummy_user_data = {
    'username': 'crimson',
    'email': 'crimson.cassanova@nowhere.com',
    'password': 'password',
    'first_name': 'Crimson',
    'last_name': 'Cassanova',
    'is_staff': True,
    'groups': ['Admins', 'Owners',],
    'a_number': 3,
    'a_string': 'Ana are mere',
    'a_date': str(datetime.date.today()),
}


class UserViewSetTest(APITestCase):

    def setUp(self):
        # create 3 test users
        for key in range(0, 3):
            User.objects.create_user(
                username='user%d' % key,
                password='password%d' % key,
                email='john.doe.%d@nowhere.com' % key,
            )
        # make a staff user
        self.staff_user = User.objects.get(username='user0')
        self.staff_user.is_staff = True
        self.staff_user.save()
        # select a test user
        self.test_user = User.objects.get(username='user1')
        # create a couple of groups
        Group.objects.bulk_create([
            Group(name='Admins'),
            Group(name='Owners'),
            Group(name='Tenants'),
        ])

    def tearDown(self):
        User.objects.all().delete()

    def test_001_retrieve_user_list(self):
        self.client.force_authenticate(self.staff_user)
        response = self.client.get(reverse('user-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), User.objects.all().count())

    def test_002_create_user(self):
        self.client.force_authenticate(self.staff_user)
        data = dummy_user_data.copy()
        response = self.client.post(reverse('user-list'), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_003_create_duplicate_user(self):
        self.client.force_authenticate(self.staff_user)
        data = dummy_user_data.copy()
        data['username'] = 'user0'
        response = self.client.post(reverse('user-list'), data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_004_retrieve_user(self):
        self.client.force_authenticate(self.staff_user)
        response = self.client.get(reverse('user-detail', args=[self.test_user.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_005_update_user(self):
        self.client.force_authenticate(self.staff_user)
        response = self.client.put(
            reverse('user-detail', args=[self.test_user.id]),
            data=dummy_user_data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        keys = [
            'username', 'email', 'first_name', 'last_name', 'is_staff',
            'groups', 'a_number', 'a_string', 'a_date',
        ]
        for key in keys:
            self.assertEqual(response.data[key], dummy_user_data[key])

    def test_006_update_duplicate_user(self):
        self.client.force_authenticate(self.staff_user)
        data = dummy_user_data.copy()
        data['username'] = self.staff_user.username
        response = self.client.put(
            reverse('user-detail', args=[self.test_user.id]),
            data=data
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_007_partial_update(self):
        self.client.force_authenticate(self.staff_user)
        response = self.client.patch(
            reverse('user-detail', args=[self.test_user.id]),
            {
                'groups': ['Tenants']
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['groups']), 1)

    def test_008_delete_user(self):
        self.client.force_authenticate(self.staff_user)
        response = self.client.delete(
            reverse('user-detail', args=[self.test_user.id])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(User.objects.filter(id=self.test_user.id).exists(), False)
        response = self.client.delete(
            reverse('user-detail', args=[self.test_user.id])
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_010_access_by_non_staff_users(self):
        self.client.force_authenticate(self.test_user)
        response = self.client.get(reverse('user-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.post(reverse('user-list'), dummy_user_data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.put(
            reverse('user-detail', args=[self.staff_user.id]),
            dummy_user_data
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.patch(
            reverse('user-detail', args=[self.staff_user.id]),
            {
                'password': 'abracadabra'
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.delete(
            reverse('user-detail', args=[self.staff_user.id]),
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ProfileViewTest(APITestCase):

    def setUp(self):
        # create one test users
        self.test_user = User.objects.create_user(
            username='john.doe',
            password='password',
            email='john.doe@nowhere.com',
        )

    def tearDown(self):
        User.objects.all().delete()

    def test_001_retrieve_profile(self):
        self.client.force_authenticate(self.test_user)
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_002_update_profile(self):
        self.client.force_authenticate(self.test_user)
        data = {
            'username': 'crimson',
            'email': 'crimson.cassanova@nowhere.com',
            'password': 'password',
            'first_name': 'Crimson',
            'last_name': 'Cassanova',
            'a_number': 3,
            'a_string': 'Ana are mere',
            'a_date': datetime.date.today(),
        }
        response = self.client.put(reverse('profile'), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_003_partial_update(self):
        self.client.force_authenticate(self.test_user)
        data = {
            'username': 'crimson',
            'first_name': 'Crimson',
            'last_name': 'Cassanova',
        }
        response = self.client.patch(reverse('profile'), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_004_update_read_only_fields(self):
        self.client.force_authenticate(self.test_user)
        data = {
            'username': 'crimson',
            'email': 'crimson.cassanova@nowhere.com',
            'password': 'password',
            'first_name': 'Crimson',
            'last_name': 'Cassanova',
            'a_number': 3,
            'a_string': 'Ana are mere',
            'a_date': datetime.date.today(),
            'is_staff': True
        }
        response = self.client.put(reverse('profile'), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['is_staff'], False)


class AuthenticationTest(APITestCase):

    def setUp(self):
        # create one test users
        self.test_user = User.objects.create_user(
            username='john.doe',
            password='password',
            email='john.doe@nowhere.com',
        )

    def tearDown(self):
        User.objects.all().delete()

    def test_001_login(self):
        response = self.client.post(reverse('login'), {
            'username': 'john.doe',
            'password': 'password'
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.post(reverse('login'), {
            'username': 'john.doe',
            'password': 'wrong password'
        })
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_002_logout(self):
        response = self.client.post(reverse('logout'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.test_user)
        response = self.client.post(reverse('logout'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
