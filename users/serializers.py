#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import transaction
from django.contrib.auth.models import User, Group
from rest_framework import serializers

from users.models import Profile


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = (
            'a_number',
            'a_string',
            'a_date',
        )


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(
        required=False, write_only=True, allow_null=True
    )
    token = serializers.CharField(source='auth_token.key', read_only=True)
    groups = serializers.SlugRelatedField(
        slug_field='name', queryset=Group.objects.all(), many=True
    )
    a_number = serializers.IntegerField(source='profile.a_number')
    a_string = serializers.CharField(source='profile.a_string')
    a_date = serializers.DateField(source='profile.a_date')

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'password',
            'token',
            'first_name',
            'last_name',
            'is_staff',
            'is_active',
            'date_joined',
            'groups',
            'a_number',
            'a_string',
            'a_date',
        )

    def validate(self, attrs):
        # is it new ?
        if not self.instance:
            # there should be a password specified
            if 'password' not in attrs or len(attrs['password']) == 0:
                raise serializers.ValidationError(
                    'Specify password when creating user'
                )
        return attrs

    @transaction.atomic()
    def create(self, validated_data):
        # poke the extra data
        profile_data = validated_data.pop('profile')
        groups = validated_data.pop('groups')
        # create the user and the profile entry (via the db signal hook)
        is_staff = validated_data.pop('is_staff')
        if is_staff:
            user = User.objects.create_superuser(**validated_data)
        else:
            user = User.objects.create_user(**validated_data)
        # update the groups
        user.groups = groups
        # update the profile
        profile_serializer = ProfileSerializer(instance=user, data=profile_data)
        profile_serializer.is_valid(raise_exception=True)
        profile_serializer.save()
        # return the new instance
        return user

    @transaction.atomic()
    def update(self, user, validated_data):
        # update the password
        if 'password' in validated_data:
            password = validated_data.pop('password')
            user.set_password(password)
        # update the groups
        if 'groups' in validated_data:
            user.groups = validated_data.pop('groups')
        # update the profile
        if 'profile' in validated_data:
            profile_data = validated_data.pop('profile')
            profile_serializer = ProfileSerializer(
                instance=user.profile, data=profile_data, partial=self.partial
            )
            profile_serializer.is_valid(raise_exception=True)
            profile_serializer.save()
        # update the user with the rest of fields
        for key, value in validated_data.items():
            setattr(user, key, value)
        # save the user
        user.save()
        # return the instance
        return user


class UserSelfSerializer(UserSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'password',
            'token',
            'first_name',
            'last_name',
            'is_staff',
            'is_active',
            'date_joined',
            'groups',
            'a_number',
            'a_string',
            'a_date',
        )
        read_only_fields = (
            'is_staff',
            'is_active',
        )


class UserProtectedSerializer(UserSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'password',
            'token',
            'first_name',
            'last_name',
            'is_staff',
            'is_active',
            'date_joined',
            'groups',
            'a_number',
            'a_string',
            'a_date',
        )
        read_only_fields = (
            'is_staff',
            'is_active',
            'groups',
        )