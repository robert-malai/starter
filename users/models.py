#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


class Profile(models.Model):
    """
    Model to store user extended data, so that we don't alter the auth User
    model;
    """
    user = models.OneToOneField(User, related_name='profile')
    # further users settings/properties goes here
    # robust design will suggest ot have default values for all the fields
    a_number = models.IntegerField(null=True)
    a_string = models.CharField(max_length=settings.MAX_NAME_FIELD_LENGTH, null=True)
    a_date = models.DateField(null=True)


@receiver(post_save, sender=User)
def create_associated_user_models(sender, instance, signal, created, **kwargs):
    # is this a new user ?
    if created:
        # create the token
        Token.objects.create(user=instance)
        # create the user profile
        Profile.objects.create(user=instance)