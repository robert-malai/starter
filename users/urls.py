#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from rest_framework import routers

from users.views import UserViewSet, login, logout, get_group_list, ProfileView

router = routers.SimpleRouter()

router.register(r'users', UserViewSet, base_name='user')

urlpatterns = patterns(
    '',
    url(r'^users/login/$', login, name='login'),
    url(r'^users/logout/$', logout, name='logout'),
    url(r'^users/groups/$', get_group_list, name='groups'),
    url(r'^users/profile/$', ProfileView.as_view(), name='profile'),
)

urlpatterns += router.urls