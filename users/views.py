#!/usr/bin/python
# -*- coding: utf-8 -*-

from rest_framework import viewsets, status, filters
from rest_framework.viewsets import generics
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, Group
from django.contrib import auth

from starter.pagination import BackbonePageableCollectionPagination
from starter.filters import BackbonePageableCollectionOrderingFilter
from users.serializers import (
    UserSerializer, UserSelfSerializer, UserProtectedSerializer,
)


@csrf_exempt
@api_view(['POST'])
@permission_classes([AllowAny])
def login(request):
    # test for credentials data
    if all(key in request.data for key in ('username', 'password')):
        # try to authenticate the user
        user = auth.authenticate(
            username=request.data['username'],
            password=request.data['password']
        )
        if user is not None:
            # test to see if the user is still active
            if user.is_active:
                # login the user (session authentication support)
                auth.login(request, user)
                # return the associated user profile
                serializer = UserSerializer(instance=user)
                return Response(data=serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def logout(request):
    auth.logout(request)
    return Response(status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_profile(request):
    # get the data
    user_serializer = UserSerializer(instance=request.user)
    # return the data
    return Response(data=user_serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_group_list(request):
    # get the data
    groups = Group.objects.all().values_list("name", flat=True)
    # return the data
    return Response(data=groups, status=status.HTTP_200_OK)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    pagination_class = BackbonePageableCollectionPagination
    permission_classes = [
        IsAdminUser
    ]
    filter_backends = (
        filters.SearchFilter,
        BackbonePageableCollectionOrderingFilter,
    )
    search_fields = (
        'username',
        'email',
        'first_name',
        'last_name'
    )
    ordering_fields = (
        'username',
        'email',
        'first_name',
        'last_name',
        'is_active',
        'is_staff'
    )

    def get_serializer_class(self):
        # are we on a edit route ?
        if 'pk' in self.kwargs:
            # if we're editing the same user as the authenticated one, then
            # we're going to return a limited serializer
            if int(self.kwargs['pk']) == self.request.user.id:
                return UserSelfSerializer
        # default serializer
        return UserSerializer


class ProfileView(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserProtectedSerializer
    permission_classes = [
        IsAuthenticated
    ]

    def get_object(self):
        # we only operate on the current user
        return self.request.user
