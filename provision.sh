#!/bin/bash
DATABASE_NAME=starter
DATABASE_USER=vagrant
DATABASE_PWD=vagrant
VIRTUAL_ENV=veStarter

echo ==========================================================================
echo Care to update the catalogs
echo ==========================================================================
apt-get update
apt-get upgrade -y

echo ==========================================================================
echo Install and configure PostgresSql
echo ==========================================================================
# add postgres PPA
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update
apt-get install -y postgresql postgresql-contrib libpq-dev
echo Enable remote access to the server for debugging purposes
PG_HBA_CONF=$(ls /etc/postgresql/*/main/pg_hba.conf | head -1)
PG_CONF=$(ls /etc/postgresql/*/main/postgresql.conf | head -1)
sudo su -c "echo '# Enable remote access to this server for debugging purposes' >> $PG_HBA_CONF"
sudo su -c "echo 'host    all             all             10.0.2.1/24             md5' >> $PG_HBA_CONF"
sudo su -c "echo 'host    all             all         192.168.50.1/24             md5' >> $PG_HBA_CONF"
sudo su -c "sed -i 's/^\s*#*\s*\(listen_addresses\s*=\s*\).*\$/\1\x27*\x27/' $PG_CONF"
sudo service postgresql restart


echo ==========================================================================
echo Initialize the application database
echo ==========================================================================

sudo -u postgres psql -c "CREATE USER $DATABASE_USER WITH PASSWORD '$DATABASE_PWD' CREATEDB;"
sudo -u postgres psql -c "CREATE DATABASE $DATABASE_NAME WITH OWNER $DATABASE_USER ENCODING 'UTF8';"

echo ==========================================================================
echo Install Python development tools
echo ==========================================================================
apt-get install -y python-dev python3-dev python-distribute python-pip python-virtualenv

echo ==========================================================================
echo Create the virtual environment and install dependencies
echo ==========================================================================
sudo su -c "virtualenv -p /usr/bin/python3 $VIRTUAL_ENV" vagrant
sudo su -c "$VIRTUAL_ENV/bin/pip install -U setuptools" vagrant
sudo su -c "$VIRTUAL_ENV/bin/pip install -r /vagrant/requirements.txt" vagrant

echo ==========================================================================
echo Install and configure apache for development mode
echo ==========================================================================
apt-get install -y apache2 openssl libapache2-mod-wsgi-py3

service apache2 stop

a2enmod rewrite
a2enmod headers
a2enmod proxy
a2enmod proxy_http

APACHEUSR=`grep -c 'APACHE_RUN_USER=www-data' /etc/apache2/envvars`
APACHEGRP=`grep -c 'APACHE_RUN_GROUP=www-data' /etc/apache2/envvars`
if [ APACHEUSR ]; then
    sed -i 's/APACHE_RUN_USER=www-data/APACHE_RUN_USER=vagrant/' /etc/apache2/envvars
fi
if [ APACHEGRP ]; then
    sed -i 's/APACHE_RUN_GROUP=www-data/APACHE_RUN_GROUP=vagrant/' /etc/apache2/envvars
fi
sudo chown -R vagrant:www-data /var/lock/apache2

ln -s /vagrant/starter.com.conf /etc/apache2/sites-available/
a2ensite starter.com.conf

service apache2 start