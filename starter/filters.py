#!/usr/bin/python
# -*- coding: utf-8 -*-

from rest_framework.filters import OrderingFilter


class BackbonePageableCollectionOrderingFilter(OrderingFilter):
    ordering_field_param = 'sort_by'
    ordering_direction_param = 'order'

    def get_ordering(self, request, queryset, view):
        """
        We override the implementation of OrderingFilter to account for the
        second parameter sent by PageableCollection - order asc or desc, and to
        tailor for only one field sorting.
        """
        field_param = request.query_params.get(self.ordering_field_param)
        direction_param = request.query_params.get(self.ordering_direction_param, 'asc')
        if field_param:
            # only the first (and only) specified field
            field = [param.strip() for param in field_param.split(',')][0]
            # append the direction
            if direction_param == 'desc':
                field = '-' + field
            # test for invalid fields
            ordering = self.remove_invalid_fields(queryset, [field], view)
            if ordering:
                return ordering

        # No ordering was included, or all the ordering fields were invalid
        return self.get_default_ordering(view)
