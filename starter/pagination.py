#!/usr/bin/python
# -*- coding: utf-8 -*-

from rest_framework.pagination import PageNumberPagination


class BackbonePageableCollectionPagination(PageNumberPagination):
    page_size = 25
    page_query_param = 'page'
    page_size_query_param = 'per_page'
    max_page_size = 100
