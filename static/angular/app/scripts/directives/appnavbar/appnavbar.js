'use strict';

/**
 * @ngdoc directive
 * @name starterApp.directive:appNavbar
 * @description
 * # appNavbar
 */
angular.module('starterApp')
  .directive('appNavbar', function () {
    return {
      restrict: 'E',
      templateUrl: 'scripts/directives/appnavbar/appnavbar.html'
    };
  });
