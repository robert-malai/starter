'use strict';

/**
 * @ngdoc overview
 * @name starterApp
 * @description
 * # starterApp
 *
 * Main module of the application.
 */
angular
    .module('starterApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .constant('API_URLs', {
        login:        '/users/login/'
    });
