(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name starterApp.AuthService
     * @description
     * # AuthService
     * Service in the starterApp.
     */
    angular.module('starterApp')
        .service('AuthService', function ($http, $cookies, $resource) {

            // define the User Profile endpoint
            var UserProfile = $resource('/users/profile/');

            // no user by default
            this.user = undefined;

            // test to see if we have a user saved
            this.isAuthenticated = function () {
                return this.user !== 'undefined';
            };

            // post the credentials to the login url and set the returned data
            // as the authenticated user and saves the token in a cookie
            this.login = function (credentials, rememberMe) {
                var self = this;
                $http.post('/users/login/', credentials)
                    .success(function (data) {
                        //self.user = data;
                        // save the cookie
                        if (rememberMe) {
                            $cookies.put('auth_token', data.token);
                        }
                        // fetch the profile from server, so that we're having
                        // an user model ready to be used in CRUD operations;
                        // cache the response that we've already received
                        self.user = angular.extend({}, data, UserProfile.get());
                    });
            };

            // post to the logout link, waiting for the successful response
            // so that we are invalidating the user and clear the cookie
            this.logout = function () {
                var self = this;
                $http.post('/users/logout/', undefined)
                    .success(function () {
                        // clear out the user
                        self.user = undefined;
                        // delete the token
                        $cookies.remove('auth_token');
                    });
            };

        });
})();
