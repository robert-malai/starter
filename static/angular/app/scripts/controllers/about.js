'use strict';

/**
 * @ngdoc function
 * @name starterApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the starterApp
 */
angular.module('starterApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
