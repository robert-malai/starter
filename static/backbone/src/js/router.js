define([
    'backbone',

    'views/home',
    'views/users/users-list',
    'models/user',
    'views/users/user-details'
], function (Backbone,

    HomeView,
    UsersListView,
    User,
    UserDetailsView
) {
    "use strict";

    var activeView = null;

    var swapView = function (newView) {
        // remove any current active view
        if (activeView) {
            activeView.remove();
        }
        activeView = newView;
        // render the view in the main placeholder
        $('#main').append(newView.render().el);
        // chain calls
        return newView;
    };

    var Router = Backbone.Router.extend({
        // defined routes
        routes: {
            '':                 'home',
            'users':            'usersList',
            'users/(:id)':      'userDetails'
        },

        home: function () {
            swapView(new HomeView());
        },

        usersList: function () {
            var usersListView = new UsersListView();
            swapView(usersListView);
            usersListView.collection.fetch({ reset: true });
        },

        userDetails: function (id) {
            var user = new User({ id: id });
            var userDetailsView = new UserDetailsView({ model: user });
            user.fetch();
            swapView(userDetailsView);
        }

    });

    return Router;
});