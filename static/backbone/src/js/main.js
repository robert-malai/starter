require.config({
    paths: {
        "jquery": "../../bower_components/jquery/dist/jquery",
        "underscore": "../../bower_components/lodash/lodash",
        "jquery-cookie": "../../bower_components/jquery.cookie/jquery.cookie",
        "jquery-serialize-object": "../../bower_components/jquery-serialize-object/jquery.serialize-object",
        "select2": "../../bower_components/select2/dist/js/select2",
        "bootstrap":"../../bower_components/bootstrap/dist/js/bootstrap",
        "backbone": "../../bower_components/backbone/backbone",
        "backbone-paginator": "../../bower_components/backbone.paginator/lib/backbone.paginator",
        "backgrid": "../../bower_components/backgrid/lib/backgrid",
        "backgrid-select-all": "../../bower_components/backgrid-select-all/backgrid-select-all",
        "backgrid-filter": "../../bower_components/backgrid-filter/backgrid-filter",
        "backgrid-paginator": "../../bower_components/backgrid-paginator/backgrid-paginator",
        "text": "../../bower_components/text/text"
    },

    shim: {
        'jquery':                       {'exports': '$' },
        'underscore':                   {'exports': '_' },

        'jquery-cookie':                {'deps': ['jquery']},
        'jquery-serialize-object':      {'deps': ['jquery']},
        'bootstrap':                    {'deps': ['jquery']},
        'select2':                      {'deps': ['jquery']},

        'backbone':                     {'deps': ['jquery', 'underscore'], 'exports': 'Backbone'},
        'backbone-paginator':           {'deps': ['backbone']},
        'backgrid':                     {'deps': ['jquery', 'underscore', 'backbone'], 'exports': 'Backgrid'},
        'backgrid-select-all':          {'deps': ['backgrid']},
        'backgrid-filter':              {'deps': ['backgrid']},
        'backgrid-paginator':           {'deps': ['backgrid']}
    }
});

define([
    'jquery',
    'underscore',
    'backbone',
    'backgrid',
    'views/navbar',
    'router',
    'app',
    'services/authentication',
    'services/ajaxSetup',

    // one-time initialization of the plugins
    'jquery-cookie',
    'jquery-serialize-object',
    'backgrid-select-all',
    'backgrid-filter',
    'backgrid-paginator',
    'bootstrap',
    'select2'
], function ($, _, Backbone, Backgrid, Navbar, Router, app, auth, ajax) {
    "use strict";

    // select2 bootstrap theme
    $.fn.select2.defaults.set( "theme", "bootstrap" );

    // setup ajax calls
    ajax.setupAjax();
    ajax.setupSync();
    // try to authenticate
    auth.authenticate();
    // create the navbar
    app.navbar = new Navbar();
    app.navbar.render();
    // create the router
    app.router = new Router();

    Backbone.history.start();
});