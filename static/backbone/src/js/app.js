/*
define(function(require, exports, module) {
    "use strict";
    // The root path to run the application through.
    exports.root = "/";
});
*/
define([
    'jquery',
    'underscore',
    'backbone',
    'confirm',
    'text!templates/alert.ejs'
], function ($, _, Backbone, ConfirmDialog, AlertTemplate) {
    "use strict";

    var alertTemplate = _.template(AlertTemplate);
    var alertsDiv = $('#alerts');
    var modalDiv = $('#modals');

    // used as a public namespace where to store the
    // global application's objects
    var Application = function () {

        // utility function to show an alert
        this.alert = function (body, type, id) {
            // do we have to search for an already existing id ?
            if (typeof id !== 'undefined') {
                if (alertsDiv.find('#' + id).length) {
                    // exit, already exists
                    return void(0);
                }
            }
            // append the alert
            alertsDiv.append(alertTemplate({
                type: typeof type !== 'undefined' ? "alert-" + type : "",
                id: typeof id !== 'undefined' ? id : "",
                body: body
            }));
        };

        // utility function to display a confirmation modal dialog
        this.confirmDialog = function (options) {
            var dialog = new ConfirmDialog(options);
            // render and show
            modalDiv.append(dialog.render().el);
            dialog.modal('show');
        };
    };

    return new Application();
});