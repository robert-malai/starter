define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/confirm.ejs'
], function ($, _, Backbone, ConfirmationTemplate) {
    "use strict";

    var ConfirmationDialog = Backbone.View.extend({

        template: _.template(ConfirmationTemplate),

        title: 'Please confirm',
        body: 'This is a confirmation dialog',
        cancel_text: 'Cancel',
        ok_text: 'OK',
        confirmation: null,

        events: {
            'click #ok-button':         'onOkDialog',
            'shown.bs.modal':           'onModalShown',
            'hidden.bs.modal':          'onModalHidden'
        },

        initialize: function (options) {
            // copy the dialog options
            this.title = _.result(options, 'title', this.title);
            this.body = _.result(options, 'body', this.body);
            this.cancel_text = _.result(options, 'cancel_text', this.cancel_text);
            this.ok_text = _.result(options, 'ok_text', this.ok_text);
            this.confirmation = _.get(options, 'confirmation', this.confirmation);
        },

        render: function () {
            this.$el.html(this.template({
                dialog: _.pick(this, ['title', 'body', 'cancel_text', 'ok_text'])
            }));
            return this;
        },

        modal: function (command) {
            this.$('#confirmationModalDialog').modal(command);
        },

        onOkDialog: function (e) {
            this.$('#confirmationModalDialog').modal('hide');
            // serialize the form content (if any)
            var data = this.$('form').serializeObject();
            // callback
            if (_.isFunction(this.confirmation)) {
                this.confirmation(data);
            }
        },

        onModalShown: function (e) {
            // set focus to first input field (if any)
            if (this.$('input:first')) {
                this.$('input:first').focus();
            }
        },

        onModalHidden: function (e) {
            // destroy this view
            this.remove();
        }
    });

    return ConfirmationDialog;
});