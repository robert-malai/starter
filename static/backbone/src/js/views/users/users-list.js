define([
    'jquery',
    'underscore',
    'backbone',
    'backgrid',
    'models/users',
    'text!templates/users/users-list.ejs',
    'text!templates/users/cell-buttons.ejs',
    'app',
    'services/authentication'
], function ($, _, Backbone, Backgrid, UsersCollection, ViewTemplate, ButtonsTemplate, app, auth) {
    "use strict";

    var isNotSelf = function (model) {
        if (auth.isAuthenticated()) {
            return model && ( model.get('id') !== auth.user.get('id') );
        }

        return false;
    };

    var UsersList = Backbone.View.extend({

        template: _.template(ViewTemplate),

        collection: new UsersCollection(),

        events: {
            'click #remove-users':  'onRemoveUsers'
        },

        initialize: function () {
            // create the grid
            this.grid = new Backgrid.Grid({
                row: Backgrid.Row.extend({
                    render: function () {
                        // super
                        Backgrid.Row.prototype.render.apply(this, arguments);
                        if (this.model.get('is_staff')) {
                            this.$el.addClass('admin-row');
                        }
                        // if it's the auth user, remove the select checkbox
                        if (auth.isAuthenticated && (this.model.get('id') === auth.user.get('id'))) {
                            this.$('td:first').empty();
                        }
                        return this;
                    }
                }),
                columns: [
                    {
                        name:       "",
                        cell:       "select-row",
                        headerCell: "select-all"
                    },
                    {
                        name:       "username",
                        label:      "User Name",
                        cell:       "string"
                    },
                    {
                        name:       "first_name",
                        label:      "First Name",
                        cell:       "string"
                    },
                    {
                        name:       "last_name",
                        label:      "Last Name",
                        cell:       "string"
                    },
                    {
                        name:       "email",
                        label:      "E-mail",
                        cell:       "email"
                    },
                    {
                        name:       "is_active",
                        label:      "Active",
                        cell:       "boolean",
                        editable:   isNotSelf
                    },
                    {
                        name:       "is_staff",
                        label:      "Staff",
                        cell:       "boolean",
                        editable:   isNotSelf
                    },
                    {
                        name:       "",
                        cell:       Backgrid.Cell.extend({
                                        template: _.template(ButtonsTemplate),
                                        render: function () {
                                            // render the template
                                            this.$el.html(this.template({ model: this.model.toJSON() }));
                                            // chaining
                                            return this;
                                        }
                                    }),
                        editable:   false
                    }
                ],
                collection: this.collection
            });
            // create the filter box
            this.filter = new Backgrid.Extension.ServerSideFilter({
                collection: this.collection,
                name: 'search',
                placeholder: 'Filter the users'
            });
            // create the paginator
            this.paginator = new Backgrid.Extension.Paginator({
                collection: this.collection
            });
            // setup listeners
            this.listenTo(this.collection, 'change', this.onCollectionChange);
            this.listenTo(this.collection, 'error', this.onServerError);
        },

        render: function () {
            // render the view
            this.$el.html(this.template());
            // render the grid
            this.$('#main-panel-body').html(this.grid.render().el);
            // add the filter
            this.$('#filter').html(this.filter.render().el);
            // add the paginator
            this.$('#main-panel-footer').html(this.paginator.render().el);
            // chain calls
            return this;
        },

        onCollectionChange: function (obj, options) {
            var model = obj, collection = obj;
            if (obj instanceof Backbone.Model) {
                // did we've changed the is_staff attribute
                if (_.has(model.changedAttributes(), 'is_staff')) {
                    var row = _.find(this.grid.body.rows, { model: model });
                    if (model.changedAttributes().is_staff) {
                        row.$el.addClass('admin-row');
                    } else {
                        row.$el.removeClass('admin-row');
                    }
                }
                // if there's no explicit save bypass
                if (!(options && options.save === false)) {
                    var attr = model.changedAttributes();
                    // update model on server
                    model.save(attr, { patch: true });
                }
            }
        },

        onServerError: function (obj, resp, options) {
            var model = obj, collection = obj;
            if (obj instanceof Backbone.Model) {
                // is it edit or new?
                if (!model.isNew()) {
                    // revert model's attributes
                    model.set(model.previousAttributes(), { silent: true });
                    // get the model's row
                    var row = _.find(this.grid.body.rows, { model: model });
                    // redraw the model with initial values
                    row.render();
                }
            }
            if (obj instanceof Backbone.Collection) {
            }
            // alert
            app.alert('<strong>Server error:</strong> ' + resp.statusText, 'danger', resp.status);
        },

        onRemoveUsers: function () {
            // get the list of selected users
            var selected = this.grid.getSelectedModels();
            // if we have some elements, then proceed
            if (selected.length > 0) {
                var compiled = _.template(
                    'You are about to delete <%= users %> user(s) with their ' +
                    'associated data. Are you sure you want to proceed?'
                );
                app.confirmDialog({
                    title: 'Delete users confirmation',
                    body: compiled({ users: selected.length }),
                    confirmation: function () {
                        // delete the users
                        _.each(selected, function (model) {
                            model.destroy({ wait: true });
                        });
                    }
                });
            }
        },

        remove: function () {
            // destroy the grid
            this.grid.remove();
            // destroy the filter
            this.filter.remove();
            // destroy the paginator
            this.paginator.remove();
            // super
            Backbone.View.prototype.remove.apply(this, arguments);
        }
    });

    return UsersList;
});