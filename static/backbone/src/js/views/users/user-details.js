define([
    'jquery',
    'underscore',
    'backbone',
    'models/user',
    'const',
    'services/authentication',
    'app',
    'text!templates/users/user-details.ejs',
    'text!templates/users/reset-password.ejs'
], function ($, _, Backbone, User, Const, auth, app, Template, ResetPasswordTemplate) {
    "use strict";

    var resetPasswordTemlate = _.template(ResetPasswordTemplate);

    var UserDetailsView = Backbone.View.extend({

        template: _.template(Template),

        events: {
            'click #reset-password':    'onResetPassword',
            'submit #user-form':        'onFormSubmit'
        },

        initialize: function () {
            this.listenTo(this.model, 'sync', this.render);
        },

        render: function () {
            // render the template
            this.$el.html(this.template({
                model: this.model.toJSON(),
                isSelf: !this.model.isNew() && this.model.get("id") === auth.user.get("id")
            }));

            // initialize the select2 control; the groups are kind of static,
            // we won't query them form the back-end.
            this.$("#groups").select2({
                data: [
                    { id: "Administrators",     text: "Administrators" },
                    { id: "Accountants",        text: "Accountants" },
                    { id: "Engineers",          text: "Engineers" }
                ],
                width: null     // we need this with bootstrap
            });

            return this;
        },

        onResetPassword: function (e) {
            var self = this;
            // confirmation dialog
            app.confirmDialog({
                title: "Reset Password",
                body: resetPasswordTemlate(),
                ok_text: "Reset",
                confirmation: function (data) {
                    self.model.save(
                        { password: data.password },
                        { patch: true, wait: true }
                    ).fail(function (xhr /*, status, error */) {
                        // report the error
                        app.alert(xhr.responseText, 'danger', xhr.status);
                    });
                }
            });
        },

        onFormSubmit: function (e) {
            // prevent default handling
            e.preventDefault();
            // get the attributes
            var attr = this.$('#user-form').serializeObject();
            // get the selected groups
            attr.groups = this.$('#groups').select2('val');
            // save the model
            this.model.save(attr, { wait: true })
                .done(function (/* data */) {
                    // navigate back
                    Backbone.history.history.back();
                })
                .fail(function (xhr /*, status, error */) {
                    // report the error
                    app.alert(xhr.responseText, 'danger', xhr.status);
                });
        }

    });

    return UserDetailsView;
});