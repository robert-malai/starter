define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/home.ejs'
], function ($, _, Backbone, Template) {
    "use strict";

    var HomeView = Backbone.View.extend({

        template: _.template(Template),

        render: function () {
            this.$el.html(this.template({}));

            return this;
        }

    });

    return HomeView;
});