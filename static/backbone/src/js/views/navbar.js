define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'services/authentication',
    'text!templates/navbar.ejs'
], function ($, _, Backbone, app, auth, Template) {
    "use strict";

    var NavbarView = Backbone.View.extend({

        el: "#navbar",

        template: _.template(Template),

        events: {
            "submit #loginForm": 'login',
            "click #logout": 'logout'
        },

        initialize: function () {
            // listen for the first authentication event.
            this.listenTo(auth, "authentication", this.render);
            this.listenTo(auth, "logout", this.render);
        },

        render: function () {
            // render the content
            var html = this.template({
                isAuthenticated: auth.isAuthenticated(),
                user: auth.user.toJSON()
            });
            this.$el.html(html);

            return this;
        },

        login: function (e) {
            // prevent form submitting
            e.preventDefault();
            // get the button
            var btnSubmit = this.$('#submit');
            btnSubmit.button('loading');
            // get the credentials
            var credentials = this.$('#loginForm').serializeObject();
            var rememberMe = _.has(credentials, 'remember');
            credentials = _.pick(credentials, ['username', 'password']);
            // call the service
            var self = this;
            auth.login(credentials, rememberMe)
                .done(function (data) {
                    // redraw the navbar
                    self.render();
                })
                .fail(function (xhr /*, status, error */) {
                    switch (xhr.status) {
                        //case 0:
                        //    app.alert('<strong>Login error:</strong> empty response', 'danger', 0);
                        //    break;
                        case 401:
                            app.alert('<strong>Login error:</strong> wrong username or password', 'danger', 401);
                            break;
                        case 403:
                            app.alert('<strong>Login error:</strong> account expired', 'danger', 403);
                            break;
                        default:
                            app.alert('<strong>Login error:</strong> server error', 'danger', 500);
                    }
                })
                .always(function () {
                    // reset button status
                    btnSubmit.button('reset');
                });
        },

        logout: function (e) {
            // forward the call to the service
            auth.logout();
            // we don't prevent the default, because we want to navigate home
            // e.preventDefault();
        }

    });

    return NavbarView;
});