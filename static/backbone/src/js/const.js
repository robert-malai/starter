define([
], function () {
    var SERVER_URL = '/api';
    return {
        SESSION_COOKIE_NAME: 'sessionid',
        CSRF_COOKIE_NAME: 'csrftoken',
        TOKEN_COOKIE_NAME: 'token',
        ROOT_API_URL: SERVER_URL,

        APIs: {
            profile:        SERVER_URL + '/users/profile/',
            login:          SERVER_URL + '/users/login/',
            logout:         SERVER_URL + '/users/logout/',
            users:          SERVER_URL + '/users/',
            groups:         SERVER_URL + '/users/groups/'
        }
    };
});