define([
    'jquery',
    'underscore',
    'backbone',
    'const'
], function ($, _, Backbone, Const) {

    // these methods don't need csrf header
    var isCsrfSafeMethod = /^(GET|HEAD|OPTIONS|TRACE)$/;

    var setupAjax = function () {
        // Setup the AJAX calls
        $.ajaxSetup({
            // enable authentication
            xhrFields: { withCredentials: true },

            // setup csrf handling and authentication token
            beforeSend: function (xhr, settings) {
                // CSRF token
                if (!isCsrfSafeMethod.test(settings.type) &&
                    $.cookie(Const.CSRF_COOKIE_NAME)) {
                    xhr.setRequestHeader("X-CSRFToken", $.cookie(Const.CSRF_COOKIE_NAME));
                }

                // Authentication Token
                if ($.cookie(Const.TOKEN_COOKIE_NAME)) {
                    xhr.setRequestHeader("Authorization", "Token " +
                        $.cookie(Const.TOKEN_COOKIE_NAME));
                }
            }
        });
    };

    var _sync = void(0);
    
    var sync = function (method, model, options) {
        // Add trailing slash to backbone model views
        var _url = _.isFunction(model.url) ?  model.url() : model.url;
        _url += _url.charAt(_url.length - 1) == '/' ? '' : '/';

        options.url = _url;

        return _sync(method, model, options);
    };

    var setupSync = function () {
        _sync = Backbone.sync;
        Backbone.sync = sync;
    };

    return {
        setupAjax: setupAjax,
        setupSync: setupSync
    };
});