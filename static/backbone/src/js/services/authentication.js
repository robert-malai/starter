define([
    'jquery',
    'underscore',
    'backbone',
    'const'
], function ($, _, Backbone, Const) {

    // The following service is responsible with login/logout and authenticate.
    // It will care for the user data and will fire authentication events.

    // special kind of user, with url pointing to profile instead of the users
    // collection based on ID.
    var UserProfile = Backbone.Model.extend({
        url: function () {
            return Const.APIs.profile;
        }
    });

    var AuthenticationService = function () {

        this.user = new UserProfile();

        // login function: will fire up the authentication AJAX call and will
        // forward the promise object to the caller
        this.login = function (credentials, rememberMe) {
            rememberMe = rememberMe || false;
            var self = this;
            return $.post(Const.APIs.login, credentials)
                .done(function (data) {
                    console.log('Login successful.');
                    // load the user data:
                    self.user.set(data);
                    // save the token as cookie
                    $.cookie(Const.TOKEN_COOKIE_NAME, self.user.get('token'), {
                        expires: rememberMe ? 15 : 0
                    });
                    // trigger the login event
                    self.trigger('login');
                });
        };

        this.authenticate = function () {
            // launch a request for the profile, with the local stored token
            // form the token cookie.
            if ($.cookie(Const.TOKEN_COOKIE_NAME)) {
                var self = this;
                // make the call via the user profile object
                self.user.fetch()
                    .done(function (data) {
                        console.log("Authentication successful.");
                        // trigger the authentication event
                        self.trigger('authentication');
                    })
                    .fail(function (xhr /*, status, error */) {
                        console.log('Authentication unsuccessful: ', xhr.responseText);
                        // clear out this cookie, it's no good
                        $.removeCookie(Const.TOKEN_COOKIE_NAME);
                    });
            }
        };

        this.isAuthenticated = function () {
            return (this.user.get("id") !== undefined);
        };

        this.logout = function () {
            // We need to make a post to the server which will invalidate our
            // user and then to clear out the AJAX calls setup.
            var self = this;
            return $.post(Const.APIs.logout)
                .always(function (data) {
                    // Clear out the user data
                    self.user.clear();
                    // clear out the cookie
                    $.removeCookie(Const.TOKEN_COOKIE_NAME);
                    // trigger the logout event
                    self.trigger('logout');
                });
        };
    };
    _.extend(AuthenticationService.prototype, Backbone.Events);

    return new AuthenticationService();
});