define([
    'jquery',
    'underscore',
    'backbone',
    'backbone-paginator'
], function ($, _, Backbone) {
    "use strict";

    var Collection = Backbone.PageableCollection.extend({

        mode: 'server',

        state: {
           pageSize: 20
        },

        queryParams: {
            totalPages:     null,
            totalRecords:   null
        },

        initialize: function () {
            // super
            Backbone.PageableCollection.prototype.initialize.apply(this, arguments);
            // https://github.com/wyuenho/backgrid/issues/453#issuecomment-56760096
            this.on('backgrid:sort', function (model) {

                // No ids so identify model with CID
                var cid = model.cid;

                var filtered = model.collection.filter(function(model) {
                    return model.cid !== cid;
                });

                _.each(filtered, function(model) {
                    model.set('direction', null);
                });
            });
        },
        
        parseState: function (resp, queryParams, state, options) {
            return {
                totalRecords:   resp.count
            };
        },

        parseLinks: function (resp, options) {
            return {
                next: resp.next,
                prev: resp.prev
            };
        },

        parseRecords: function (resp, options) {
            return resp.results;
        },

        hasPrevious: function () {
            return this.hasPreviousPage();
        },

        hasNext: function () {
            return this.hasNextPage();
        }

    });

    return Collection;
});