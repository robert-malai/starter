define([
    'jquery',
    'underscore',
    'backbone',
    'models/user',
    'models/collection',
    'const'
], function ($, _, Backbone, UserModel, Collection, Const) {
    "use strict";

    var UsersCollection = Collection.extend({

        model: UserModel,

        url: Const.APIs.users

    });

    return UsersCollection;
});