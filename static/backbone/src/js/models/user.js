define([
    'jquery',
    'underscore',
    'backbone',
    'const'
], function ($, _, Backbone, Const) {
    "use strict";

    var UserModel = Backbone.Model.extend({
        defaults: {
            'username': null,
            'email': null,
            'password': null,
            'token': null,
            'first_name': null,
            'last_name': null,
            'is_staff': false,
            'is_active': true,
            'date_joined': null,
            'groups': [],
            'a_number': null,
            'a_string': null,
            'a_date': null
        },

        urlRoot: Const.APIs.users
    });

    return UserModel;
});