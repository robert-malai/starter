module.exports = function (grunt) {

    var path = require('path');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        cssmin: {
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.css': ['<%= css_files %>']
                }
            }
        },
        requirejs: {
            compile: {
                options: {
                    name:                   '../../bower_components/almond/almond',
                    baseUrl:                'src/js',
                    mainConfigFile:         'src/js/main.js',
                    include:                ['main'],
                    insertRequire:          ['main'],
                    out:                    'dist/<%= pkg.name %>.min.js',
                    wrapShim:               true,
                    removeCombined:         true,
                    findNestedDependencies: true,
                    optimize:               'uglify2',
                    uglify2: {
                        output: {
                            beautify:       false
                        },
                        compress: {
                            unsafe:         true,
                            sequences:      true,
                            properties:     true,
                            evaluate:       true,
                            dead_code:      true,
                            conditionals:   true,
                            booleans:       true,
                            loops:          true,
                            unused:         true,
                            if_return:      true,
                            join_vars:      true,
                            cascade:        true,
                            negate_iife:    true
                        },
                        warnings:           true,
                        mangle:             true
                    }
                }
            }
        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        flatten: true,
                        src: ['**/bootstrap/fonts/*'],
                        dest: 'dist/fonts/'
                    },
                    {
                        expand: true,
                        dot: true,
                        flatten: true,
                        src: ['src/img/*'],
                        dest: 'dist/img/'
                    },
                    {
                        expand: true,
                        dot: true,
                        flatten: true,
                        src: 'src/favicon.ico',
                        dest: 'dist/'
                    }
                ]
            }
        },
        processhtml: {
            dist: {
                options: {
                    process: true,
                    data: {
                        'pkg': '<%= pkg %>'
                    }
                },
                files: {
                    'dist/index.html': ['src/**/index.html']
                }
            }
        }
    });

    grunt.registerTask('scan-css', function () {
        // search the index file
        var index_path = grunt.file.expand('src/**/index.html')[0];
        if (index_path) {
            var index_dir = path.dirname(index_path);
            grunt.verbose.writeln('Found index file: ' + index_path);
            // load the file into memory
            var index_html = grunt.file.read(index_path);
            // search for the processhtml pattern involving css
            var css_pattern = /^\s*<!--\s+build:css\s+(?:.*)\.css\W*(?:.*)-->\s*$([\s\S]*?)^\s*<!--\s*\/build\s*-->\s*$/m;
            var result;
            if ((result = css_pattern.exec(index_html)) !== null) {
                var links_html = result[1];
                grunt.verbose.write('Identified CSS links:');
                grunt.verbose.writeln(links_html);
                // search for all the href properties
                var css_files = [];
                var href_pattern = /href=["'](.*?)['"]/g;
                while ((result = href_pattern.exec(links_html)) !== null) {
                    css_files.push(path.join(index_dir, result[1]));
                }
                grunt.verbose.writeln('Identified CSS files:');
                css_files.forEach(function (element) {
                    grunt.verbose.writeln('\t' + element);
                });
                // set the config resource
                grunt.config('css_files', css_files);
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-processhtml');

    grunt.registerTask('default', ['jshint']);
    grunt.registerTask('build', [
        'jshint',
        'scan-css',
        'cssmin',
        'requirejs',
        'processhtml',
        'copy'
    ]);

};
